<?php

use Illuminate\Database\Seeder;

class AuthUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('api_users')->truncate();

        $user = new \App\Models\ApiUser();
        $user->name = "ProPass";
        $user->surname = "ApiCreator";
        $user->username = "propassapi";
        $user->password = bcrypt("123456");
        $user->save();
    }
}
