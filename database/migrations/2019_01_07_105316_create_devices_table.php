<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('device_name',60);
            $table->unsignedInteger('device_manufacture_id');
            $table->unsignedInteger('device_model_id');
            $table->unsignedInteger('device_type_id');
            $table->unsignedInteger('device_destination_id');
            $table->ipAddress('ip_address');
            $table->string('serial_number',60);
            $table->string('google_map',120);
            $table->text('note');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('device_manufacture_id')->references('id')->on('device_manufactures');
            $table->foreign('device_model_id')->references('id')->on('device_models')->onDelete('CASCADE');
            $table->foreign('device_type_id')->references('id')->on('device_types')->onDelete('CASCADE');
            $table->foreign('device_destination_id')->references('id')->on('device_destinations')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
