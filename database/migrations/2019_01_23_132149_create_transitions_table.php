<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transitions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('device_type_id');
            $table->unsignedInteger('access_type_id');
            $table->string('location');
            $table->string('google_map');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('device_type_id')->references('id')->on('device_types')->onDelete('CASCADE');
            $table->foreign('access_type_id')->references('id')->on('access_types')->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transitions');
    }
}
