<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisteredFingersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registered_fingers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reg_id');
            $table->unsignedInteger('finger_index');
            $table->unsignedInteger('device_id');
            $table->text('fing_template');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('device_id')->references('id')->on('devices')->onDelete('CASCADE');
            $table->foreign('reg_id')->references('id')->on('registered_users')->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registered_fingers');
    }
}
