<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('device_sdk_type_id');
            $table->unsignedInteger('device_finger_code_id');
            $table->unsignedInteger('device_manufacture_id');
            $table->timestamps();

            $table->foreign('device_sdk_type_id')->references('id')->on('device_sdk_types')->onDelete('CASCADE');
            $table->foreign('device_finger_code_id')->references('id')->on('device_finger_codes')->onDelete('CASCADE');
            $table->foreign('device_manufacture_id')->references('id')->on('device_manufactures')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_models');
    }
}
