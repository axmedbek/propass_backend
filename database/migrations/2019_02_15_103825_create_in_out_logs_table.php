<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInOutLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('in_out_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reg_id');
            $table->text("finger_template")->nullable();
            $table->ipAddress('device_ip');
            $table->string('card_number',120)->nullable();
            $table->unsignedInteger('destination_id');
            $table->timestamps();

            $table->foreign('destination_id')->references('id')->on('device_destinations')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('in_out_logs');
    }
}
