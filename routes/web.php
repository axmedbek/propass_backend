<?php

// like as guest
Route::group(['middleware' => ['guest:api_web']], function () {
    Route::get('/login','Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login','Auth\LoginController@login');
});

Route::group(['middleware' => ['auth:api_web']],function(){
    Route::get('/logout','Auth\LoginController@logout')->name('api.logout');
    Route::get('/','HomeController@index')->name('api.home');
    Route::post('/oauth/token','HomeController@getToken')->name('api.token');
    Route::get('/user/oauth/token', 'UserTokenController@index')->name('api.user.tokens');

});
