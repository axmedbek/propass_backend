<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Accept, Authorization, X-Requested-With, Application');


use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login','Auth\ApiAuthenticationController@login')->name('auth.login.post');

Route::group(['middleware' => 'auth:api'], function () {

    Route::post('/register','Auth\ApiAuthenticationController@register')->name('auth.register.user');
    Route::get('/logout', 'Auth\ApiAuthenticationController@logout')->name('auth.logout.user');

    // msk
    Route::group(['prefix' => 'msk'], function () {
        Route::post('/device-manufacture/save','Msk\Devices\ManufacturerController@save')->name('api.msk.device.manufacture.save');
        Route::get('/device-manufacture/all','Msk\Devices\ManufacturerController@index')->name('api.msk.device.manufacture.all');
        Route::post('/device-manufacture/delete','Msk\Devices\ManufacturerController@delete')->name('api.msk.device.manufacture.delete');

        Route::group(['prefix' => 'access'], function () {
            // device types
            Route::post('/access-types/save','Msk\Access\AccessTypeController@save')->name('api.msk.access.type.save');
            Route::get('/access-types/all','Msk\Access\AccessTypeController@index')->name('api.msk.access.type.all');
            Route::post('/access-types/delete','Msk\Access\AccessTypeController@delete')->name('api.msk.access.type.delete');

        });

        // device types
        Route::post('/device-types/save','Msk\Devices\DeviceTypeController@save')->name('api.msk.device.type.save');
        Route::get('/device-types/all','Msk\Devices\DeviceTypeController@index')->name('api.msk.device.type.all');
        Route::post('/device-types/delete','Msk\Devices\DeviceTypeController@delete')->name('api.msk.device.type.delete');

        // device destinations
        Route::post('/device-destination/save','Msk\Devices\DestinationController@save')->name('api.msk.device.destination.save');
        Route::get('/device-destination/all','Msk\Devices\DestinationController@index')->name('api.msk.device.destination.all');
        Route::post('/device-destination/delete','Msk\Devices\DestinationController@delete')->name('api.msk.device.destination.delete');

        // device sdk types
        Route::post('/device-sdk-types/save','Msk\Devices\SdkTypeController@save')->name('api.msk.device.sdk_types.save');
        Route::get('/device-sdk-types/all','Msk\Devices\SdkTypeController@index')->name('api.msk.device.sdk_types.all');
        Route::post('/device-sdk-types/delete','Msk\Devices\SdkTypeController@delete')->name('api.msk.device.sdk_types.delete');

        // device finger code types
        Route::post('/device-finger-code-types/save','Msk\Devices\FingerCodeTypeController@save')->name('api.msk.device.finger_code_types.save');
        Route::get('/device-finger-code-types/all','Msk\Devices\FingerCodeTypeController@index')->name('api.msk.device.finger_code_types.all');
        Route::post('/device-finger-code-types/delete','Msk\Devices\FingerCodeTypeController@delete')->name('api.msk.device.finger_code_types.delete');


        // device models
        Route::post('/device-models/save','Msk\Devices\DeviceModelController@save')->name('api.msk.device.model.save');
        Route::post('/device-models/all','Msk\Devices\DeviceModelController@index')->name('api.msk.device.model.all');
        Route::post('/device-models/delete','Msk\Devices\DeviceModelController@delete')->name('api.msk.device.model.delete');

        // finger operation Filter types
        Route::post('/fp-operation/save','Msk\Devices\DeviceFingerOperationController@save')->name('api.msk.fp-operation.save');
        Route::get('/fp-operation/all','Msk\Devices\DeviceFingerOperationController@index')->name('api.msk.fp-operation.all');
        Route::post('/fp-operation/delete','Msk\Devices\DeviceFingerOperationController@delete')->name('api.msk.fp-operation.delete');
    });
//    devices api
    Route::group(['prefix' => 'device'], function () {
        Route::post('/save','DeviceController@save')->name('api.device.save');
        Route::get('/all','DeviceController@index')->name('api.device.all');
        Route::post('/get','DeviceController@getDeviceById')->name('api.device.get');
        Route::get('/get/devices_with_destination/{id}','DeviceController@getDeviceByDestinationId')->where('id','[0-9]+')->name('api.device.with.destination');
        Route::post('/get/devices-with-department-id','DeviceController@getDevicesByDepartmentId')->name('api.devices.with.department.get');
    });

//    fp api
    Route::group(['prefix' => 'fp'], function () {
        Route::post('/register-user','Fp\RegisterUserController@registerUser')->name('api.fp.register.user');
        Route::post('/fp-operation','Fp\FingerOperationController@addOperation')->name('api.fp.register.user');
        Route::post('/register-finger','Fp\RegisterFingerController@registerFinger')->name('api.fp.register.finger');
        Route::post('/selected-fingers','Fp\RegisterUserController@getSelectedFingers')->name('api.fp.selected.fingers');
        Route::post('/finger-templates','Fp\RegisterUserController@getFingerTemplates')->name('api.fp.finger.templates');
        Route::post('/operation-logs','Fp\FingerOperationController@getAllOperations')->name('api.fp.all.operations');
        Route::post('/register-finger-logs','Fp\RegisterFingerController@getAllFingersLogs')->name('api.fp.all.registers.logs');
        Route::post('/get/register-finger-info','Fp\RegisterFingerController@getFingerById')->name('api.fp.all.finger.get');
        Route::get('/get/register-users','Fp\RegisterUserController@getRegisteredUserWithFinger')->name('api.fp.all.users.get');
        Route::get('/get/user-fingers/{id}','Fp\RegisterUserController@getFingersWithUserId')->where('id','[0-9]+')->name('api.fp.all.fingers.with.user_id.get');
        Route::post('/get/devices-and-transitions-with-finger-code','Fp\RegisterFingerController@getDevicesAndTransitionsWithFingerCode')->name('api.fp.all.devices.and.transitions.with.finger_code');

        // in and out log
        Route::post('/in-out-log/save','Fp\FingerInOutLogController@saveInOut')->name('api.fp.in.out.log');

    });

    Route::group(['prefix' => 'transition'], function () {
        Route::get('/all','Transition\TransitionController@index')->name('api.access.all');
        Route::post('/save','Transition\TransitionController@addNewTransition')->name('api.access.save');
        Route::get('/get/{id}','Transition\TransitionController@getTransitionById')->where('id','[0-9]+')->name('api.access.save');
    });

    Route::group(['prefix' => 'sys-users'], function () {
        Route::get('/all','Auth\ApiAuthenticationController@index')->name('api.sys.users.all');
    });

    Route::group(['prefix' => 'roles'], function () {
        Route::get('/all','Role\RoleController@index')->name('api.roles.all');
        Route::post('/save','Role\RoleController@save')->name('api.roles.save');
        Route::post('/delete','Role\RoleController@delete')->name('api.roles.delete');
    });
});

