@extends('base')

@section('content')
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body"><!-- stats -->
                <section id="basic-tag-input">
                    <div class="row match-height">
                        <div class="col-xl-12 col-lg-12">
                            <div id="devices" class="scroll_finder"></div>
                            <div class="card" style="height: 340px;">
                                <div class="card-header">
                                    <h4 class="card-title">Qurğular</h4>
                                </div>
                                <div class="card-body">
                                    <div class="card-block">
                                        <p>Takes the basic nav from above and adds the <code>.nav-tabs</code> class to generate a tabbed interface. </p>
                                        <div class="tab-content px-1 pt-1">
                                           <h6>Devices</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="fingers" class="scroll_finder"></div>
                            <div class="card" style="height: 340px;">
                                <div class="card-header">
                                    <h4 class="card-title">Barmaq qeydiyyatı</h4>
                                </div>
                                <div class="card-body">
                                    <div class="card-block">
                                        <p>Takes the basic nav from above and adds the <code>.nav-tabs</code> class to generate a tabbed interface. </p>
                                        <div class="tab-content px-1 pt-1">
                                            <h6>Fingers</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="canteen" class="scroll_finder"></div>
                            <div class="card" style="height: 340px;">
                                <div class="card-header">
                                    <h4 class="card-title">Yemekxana</h4>
                                </div>
                                <div class="card-body">
                                    <div class="card-block">
                                        <p>Takes the basic nav from above and adds the <code>.nav-tabs</code> class to generate a tabbed interface. </p>
                                        <div class="tab-content px-1 pt-1">
                                            <h6>Canteen</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="another1" class="scroll_finder"></div>
                            <div class="card" style="height: 340px;">
                                <div class="card-header">
                                    <h4 class="card-title">Another1</h4>
                                </div>
                                <div class="card-body">
                                    <div class="card-block">
                                        <p>Takes the basic nav from above and adds the <code>.nav-tabs</code> class to generate a tabbed interface. </p>
                                        <div class="tab-content px-1 pt-1">
                                            <h6>Canteen</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="another2" class="scroll_finder"></div>
                            <div class="card" style="height: 340px;">
                                <div class="card-header">
                                    <h4 class="card-title">Another2</h4>
                                </div>
                                <div class="card-body">
                                    <div class="card-block">
                                        <p>Takes the basic nav from above and adds the <code>.nav-tabs</code> class to generate a tabbed interface. </p>
                                        <div class="tab-content px-1 pt-1">
                                            <h6>Canteen</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="modal fade text-xs-left" id="token_creator_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel1">Use this token for API Processes</h4>
                    </div>
                    <div class="modal-body">
                        <pre id="token_result_code" class="prettyprint" style="white-space: pre-wrap;">

                        </pre>
                        <button class="btn btn-danger copyBtn" data-clipboard-text="">
                            <i class="icon icon-ios-copy-outline" style="font-size: 20px;"></i>
                            <span>Tokeni kopyala</span>
                        </button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{--<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>--}}
    <script src="{{ asset('assets/js/clipboard.min.js') }}" type="text/javascript"></script>

    <script>

        var clipboard = new ClipboardJS('.copyBtn');
        $.fn.modal.Constructor.prototype._enforceFocus = function() {};

        $('#get_token_btn').click(function(){
            $.post('{{ route('api.token') }}',{ _token : _token },function(response){
                $('#token_result_code').html(syntaxHighlight(response));
                $('.copyBtn').attr('data-clipboard-text',response.access_token);
            });
        });

        function syntaxHighlight(json) {
            if (typeof json != 'string') {
                json = JSON.stringify(json, undefined, 2);
            }
            json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
            return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                var cls = 'number';
                if (/^"/.test(match)) {
                    if (/:$/.test(match)) {
                        cls = 'color:tomato';
                    } else {
                        cls = 'color:green';
                    }
                } else if (/true|false/.test(match)) {
                    cls = 'color:violent';
                } else if (/null/.test(match)) {
                    cls = 'color:#ff00c7';
                }
                return '<span style="' + cls + '">' + match + '</span>';
            });
        }

    </script>
@endsection

@section('css')
    <style>
        .scroll_finder{
            position: relative;
            top: -80px;
        }
    </style>
@endsection

@section('title') Ana səhifə @endsection
