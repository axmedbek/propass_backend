<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="axmedbek">
    <title>@yield('title',' | ProPass API')</title>
    @include('layouts.styles')
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

<!-- navbar -->
@include('layouts.navbar')
<!-- navbar-->

<!-- main menu-->
@include('layouts.sidebar')
<!-- / main menu-->

@yield('content')

@include('layouts.footer')
@include('layouts.scripts')
</body>
</html>