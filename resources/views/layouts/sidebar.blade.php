<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <div class="main-menu-header"></div>
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
            <li class=" nav-item"><a href="https://pixinvent.com/free-bootstrap-template/robust-lite/documentation"><i
                            class="icon-document-text"></i><span data-i18n="nav.support_documentation.main"
                                                                 class="menu-title">Documentation</span></a>
                <ul class="menu-content">
                    <li class="active"><a href="#devices" data-i18n="nav.dash.main" class="menu-item">Qurğular</a>
                    </li>
                    <li><a href="#fingers" data-i18n="nav.dash.main" class="menu-item">Barmaq qeydiyyatı</a>
                    </li>
                    <li><a href="#canteen" data-i18n="nav.dash.main" class="menu-item">Yemekxana</a>
                    </li>
                    <li><a href="#another1" data-i18n="nav.dash.main" class="menu-item">Another 1</a>
                    </li>
                    <li><a href="#another2" data-i18n="nav.dash.main" class="menu-item">Another 2</a>
                    </li>
                </ul>
            </li>
            {{--<li class="nav-item"><a href="{{ route('api.user.tokens') }}"><i class="icon-paper"></i><span--}}
                            {{--data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">User Tokens</span></a>--}}
            {{--</li>--}}
        </ul>
    </div>
</div>