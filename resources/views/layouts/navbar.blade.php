<nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a
                            class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a>
                </li>
                <li class="nav-item"><a href="{{ route('api.home') }}" class="navbar-brand nav-link">
                        <img alt="branding logo" src="{{ asset('assets/images/white_logo2.png')  }}"
                             data-expand="{{ asset('assets/images/white_logo2.png')  }}"
                             data-collapse="{{ asset('assets/images/white_logo2.png')  }}"
                             class="brand-logo" style="width: 180px;"></a>
                </li>

            </ul>
        </div>
        <div class="navbar-container content container-fluid">
            <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                <ul class="nav navbar-nav float-xs-right">
                    <li class="nav-item hidden-sm-down">
                        <a id="get_token_btn" href="javascript:void(0)" data-toggle="modal" data-target="#token_creator_modal"
                                 class="btn btn-success upgrade-to-pro">
                            <img style="width: 20px;margin-right: 6px;" src="{{ asset('assets/images/token.png') }}" alt="token icon">
                            TOKEN yarat
                        </a>
                    </li>
                    <li class="dropdown dropdown-user nav-item"><a href="#" data-toggle="dropdown"
                                                                   class="dropdown-toggle nav-link dropdown-user-link"><span
                                    class="avatar avatar-online"><img
                                        src="{{ asset('assets/images/user.png') }}" alt="avatar"><i></i></span><span
                                    class="user-name">{{ auth()->user()->name." ".auth()->user()->surname }}</span></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{ route('api.logout') }}" class="dropdown-item"><i class="icon-power3"></i> Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>