<?php

namespace App\Http\Controllers\Msk\Devices;

use App\Http\Controllers\BaseController;
use App\Http\Requests\OneColumnRequest;
use App\Models\Msk\DeviceDestination;
use Illuminate\Http\Request;

class DestinationController extends BaseController
{
    private $deviceDestinationObj;

    public function __construct(DeviceDestination $deviceDestinationObj)
    {
        $this->deviceDestinationObj = $deviceDestinationObj;
    }

    public function index()
    {
        return $this->sendResponse($this->deviceDestinationObj->getAllDeviceTypes());
    }

    public function save(OneColumnRequest $request)
    {

        // validation process
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        // save process
        if ($request->get('id') == 0) {
            $this->deviceDestinationObj = new DeviceDestination();
        } // edit process
        else {
            $this->deviceDestinationObj = $this->deviceDestinationObj->getDeviceTypeById($request->get('id'));
        }

        $this->deviceDestinationObj->name = $request->get('name');
        $this->deviceDestinationObj->save();

        return $this->sendResponse($this->deviceDestinationObj->getAllDeviceTypes());
    }

    public function getById(OneColumnRequest $request, $id)
    {
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        return $this->sendResponse($this->deviceDestinationObj->getDeviceTypeById($id));
    }

    public function delete(Request $request)
    {
        $validator = validator($request->all(),[
            'id' => 'required|integer|exists:device_destinations,id',
            'user_id' => 'required|integer|exists:api_users,id'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $this->deviceDestinationObj = $this->deviceDestinationObj->getDeviceTypeById($request->get('id'));
        try {
            $this->deviceDestinationObj->delete();
            return $this->sendResponse([ 'id' => $this->deviceDestinationObj->id]);
        } catch (\Exception $e) {
            return $this->sendError([
                'userMsg' => 'Oops..Something was wrong',
                'devMsg' => $e->getMessage()
            ]);
        }
    }

}
