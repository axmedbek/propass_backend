<?php

namespace App\Http\Controllers\Msk\Devices;

use App\Http\Controllers\BaseController;
use App\Http\Requests\OneColumnRequest;
use App\Models\Msk\DeviceManufacture;
use Symfony\Component\HttpFoundation\Request;


class ManufacturerController extends BaseController
{
    private $manufactureObj;

    public function __construct(DeviceManufacture $manufactureObj)
    {
        $this->manufactureObj = $manufactureObj;
    }

    public function index()
    {
        return $this->sendResponse($this->manufactureObj->getAllManufactures());
    }

    public function save(OneColumnRequest $request)
    {

        // validation process
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        // save process
        if ($request->get('id') == 0) {
            $this->manufactureObj = new DeviceManufacture();
        } // edit process
        else {
            $this->manufactureObj = $this->manufactureObj->getManufactureById($request->get('id'));
        }

        $this->manufactureObj->name = $request->get('name');
        $this->manufactureObj->save();

        return $this->sendResponse($this->manufactureObj->getAllManufactures());
    }

    public function getById(OneColumnRequest $request, $id)
    {
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        return $this->sendResponse($this->manufactureObj->getManufactureById($id));
    }

    public function delete(Request $request)
    {
        $validator = validator($request->all(),[
            'id' => 'required|integer|exists:device_manufactures,id',
            'user_id' => 'required|integer|exists:api_users,id'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $this->manufactureObj = $this->manufactureObj->getManufactureById($request->get('id'));
        try {
            $this->manufactureObj->delete();
            return $this->sendResponse([ 'id' => $this->manufactureObj->id]);
        } catch (\Exception $e) {
            return $this->sendError([
                'userMsg' => 'Oops..Something was wrong',
                'devMsg' => $e->getMessage()
            ]);
        }
    }
}
