<?php

namespace App\Http\Controllers\Msk\Devices;

use App\Http\Controllers\BaseController;
use App\Http\Requests\DeviceFingerOperationRequest;
use App\Models\Msk\DeviceFingerOperation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeviceFingerOperationController extends BaseController
{
    private $deviceOperationObj;

    public function __construct(DeviceFingerOperation $deviceOperationObj)
    {
        $this->deviceOperationObj = $deviceOperationObj;
    }

    public function index()
    {
        return $this->sendResponse($this->deviceOperationObj->getAllFingerOperation());
    }

    public function save(DeviceFingerOperationRequest $request)
    {

        // validation process
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        // save process
        if ($request->get('id') == 0) {
            $this->deviceOperationObj = new DeviceFingerOperation();
        } // edit process
        else {
            $this->deviceOperationObj = $this->deviceOperationObj->getFingerOperationById($request->get('id'));
        }

        $this->deviceOperationObj->name = $request->get('name');
        $this->deviceOperationObj->slug = $request->get('slug');
        $this->deviceOperationObj->save();

        return $this->sendResponse($this->deviceOperationObj->getAllFingerOperation());
    }

    public function getById(DeviceFingerOperationRequest $request, $id)
    {
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        return $this->sendResponse($this->deviceOperationObj->getFingerOperationById($id));
    }

    public function delete(Request $request)
    {
        $validator = validator($request->all(),[
            'id' => 'required|integer|exists:device_finger_operations,id',
            'user_id' => 'required|integer|exists:api_users,id'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $this->deviceOperationObj = $this->deviceOperationObj->getFingerOperationById($request->get('id'));
        try {
            $this->deviceOperationObj->delete();
            return $this->sendResponse([ 'id' => $this->deviceOperationObj->id]);
        } catch (\Exception $e) {
            return $this->sendError([
                'userMsg' => 'Oops..Something was wrong',
                'devMsg' => $e->getMessage()
            ]);
        }
    }
}
