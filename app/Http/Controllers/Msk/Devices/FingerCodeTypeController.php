<?php

namespace App\Http\Controllers\Msk\Devices;

use App\Http\Controllers\BaseController;
use App\Http\Requests\OneColumnRequest;
use App\Models\Msk\DeviceFingerCode;
use Illuminate\Http\Request;

class FingerCodeTypeController extends BaseController
{
    private $deviceFingerCodeTypeObj;

    public function __construct(DeviceFingerCode $deviceFingerCodeTypeObj)
    {
        $this->deviceFingerCodeTypeObj = $deviceFingerCodeTypeObj;
    }

    public function index()
    {
        return $this->sendResponse($this->deviceFingerCodeTypeObj->getAllData());
    }

    public function save(OneColumnRequest $request)
    {

        // validation process
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        // save process
        if ($request->get('id') == 0) {
            $this->deviceFingerCodeTypeObj = new DeviceFingerCode();
        } // edit process
        else {
            $this->deviceFingerCodeTypeObj = $this->deviceFingerCodeTypeObj->getDataById($request->get('id'));
        }

        $this->deviceFingerCodeTypeObj->name = $request->get('name');
        $this->deviceFingerCodeTypeObj->save();

        return $this->sendResponse($this->deviceFingerCodeTypeObj->getAllData());
    }

    public function getById(OneColumnRequest $request, $id)
    {
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        return $this->sendResponse($this->deviceFingerCodeTypeObj->getDataById($id));
    }

    public function delete(Request $request)
    {
        $validator = validator($request->all(),[
            'id' => 'required|integer|exists:device_finger_codes,id',
            'user_id' => 'required|integer|exists:api_users,id'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $this->deviceFingerCodeTypeObj = $this->deviceFingerCodeTypeObj->getDataById($request->get('id'));
        try {
            $this->deviceFingerCodeTypeObj->delete();
            return $this->sendResponse([ 'id' => $this->deviceFingerCodeTypeObj->id]);
        } catch (\Exception $e) {
            return $this->sendError([
                'userMsg' => 'Oops..Something was wrong',
                'devMsg' => $e->getMessage()
            ]);
        }
    }
}
