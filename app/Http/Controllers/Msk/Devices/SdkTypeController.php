<?php

namespace App\Http\Controllers\Msk\Devices;

use App\Http\Controllers\BaseController;
use App\Http\Requests\OneColumnRequest;
use App\Models\Msk\DeviceSdkType;
use Illuminate\Http\Request;

class SdkTypeController extends BaseController
{
    private $deviceSdkTypeObj;

    public function __construct(DeviceSdkType $deviceSdkTypeObj)
    {
        $this->deviceSdkTypeObj = $deviceSdkTypeObj;
    }

    public function index()
    {
        return $this->sendResponse($this->deviceSdkTypeObj->getAllData());
    }

    public function save(OneColumnRequest $request)
    {

        // validation process
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        // save process
        if ($request->get('id') == 0) {
            $this->deviceSdkTypeObj = new DeviceSdkType();
        } // edit process
        else {
            $this->deviceSdkTypeObj = $this->deviceSdkTypeObj->getDataById($request->get('id'));
        }

        $this->deviceSdkTypeObj->name = $request->get('name');
        $this->deviceSdkTypeObj->save();

        return $this->sendResponse($this->deviceSdkTypeObj->getAllData());
    }

    public function getById(OneColumnRequest $request, $id)
    {
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        return $this->sendResponse($this->deviceSdkTypeObj->getDataById($id));
    }

    public function delete(Request $request)
    {
        $validator = validator($request->all(),[
            'id' => 'required|integer|exists:device_sdk_types,id',
            'user_id' => 'required|integer|exists:api_users,id'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $this->deviceSdkTypeObj = $this->deviceSdkTypeObj->getDataById($request->get('id'));
        try {
            $this->deviceSdkTypeObj->delete();
            return $this->sendResponse([ 'id' => $this->deviceSdkTypeObj->id]);
        } catch (\Exception $e) {
            return $this->sendError([
                'userMsg' => 'Oops..Something was wrong',
                'devMsg' => $e->getMessage()
            ]);
        }
    }
}
