<?php

namespace App\Http\Controllers\Msk\Devices;

use App\Http\Controllers\BaseController;
use App\Http\Requests\OneColumnRequest;
use App\Models\Msk\DeviceType;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;

class DeviceTypeController extends BaseController
{

    private $deviceTypeObj;

    public function __construct(DeviceType $deviceTypeObj)
    {
        $this->deviceTypeObj = $deviceTypeObj;
    }

    public function index()
    {
        return $this->sendResponse($this->deviceTypeObj->getAllDeviceTypes());
    }

    public function save(OneColumnRequest $request)
    {

        // validation process
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        // save process
        if ($request->get('id') == 0) {
            $this->deviceTypeObj = new DeviceType();
        } // edit process
        else {
            $this->deviceTypeObj = $this->deviceTypeObj->getDeviceTypeById($request->get('id'));
        }

        $this->deviceTypeObj->name = $request->get('name');
        $this->deviceTypeObj->save();

        return $this->sendResponse($this->deviceTypeObj->getAllDeviceTypes());
    }

    public function getById(OneColumnRequest $request, $id)
    {
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        return $this->sendResponse($this->deviceTypeObj->getDeviceTypeById($id));
    }

    public function delete(Request $request)
    {
        $validator = validator($request->all(),[
            'id' => 'required|integer|exists:device_types,id',
            'user_id' => 'required|integer|exists:api_users,id'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $this->deviceTypeObj = $this->deviceTypeObj->getDeviceTypeById($request->get('id'));
        try {
            $this->deviceTypeObj->delete();
            return $this->sendResponse([ 'id' => $this->deviceTypeObj->id]);
        } catch (\Exception $e) {
            return $this->sendError([
                'userMsg' => 'Oops..Something was wrong',
                'devMsg' => $e->getMessage()
            ]);
        }
//        [ 'id' => $this->deviceTypeObj->id]
    }

}
