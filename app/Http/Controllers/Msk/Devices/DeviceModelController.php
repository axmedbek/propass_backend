<?php

namespace App\Http\Controllers\Msk\Devices;

use App\Http\Controllers\BaseController;
use App\Http\Requests\DeviceModelRequest;
use App\Models\Msk\DeviceModel;
use Symfony\Component\HttpFoundation\Request;

class DeviceModelController extends BaseController
{
    private $deviceModelObj;

    public function __construct(DeviceModel $deviceModelObj)
    {
        $this->deviceModelObj = $deviceModelObj;
    }

    public function index(Request $request){
        $validator = validator($request->all(),[
           'user_id' => 'required|integer|exists:api_users,id',
            'device_manufacture_id' => 'required|integer|exists:device_manufactures,id'
        ]);

        if ($validator->fails()){
            if ($request->get('device_manufacture_id') == 0){
                return $this->sendResponse([]);
            }
            else{
//                return $this->sendError($validator->errors());
                return $this->sendResponse([]);
            }
        }

        return $this->sendResponse($this->deviceModelObj->getAllData($request->get('device_manufacture_id')));
    }
    public function save(DeviceModelRequest $request){

        try{
            //validation process
            $validated = $request->validator;

            if ($validated->fails()){
                return $this->sendError($validated->errors());
            }

            // save process
            if ($request->get('id') == 0){
                $this->deviceModelObj = new DeviceModel();
            }
            // edit process
            else{
                $this->deviceModelObj = $this->deviceModelObj->getDataById($request->get('id'));
            }

            $this->deviceModelObj->name = $request->get('name');
            $this->deviceModelObj->device_sdk_type_id = $request->get('device_sdk_type_id');
            $this->deviceModelObj->device_finger_code_id = $request->get('device_finger_code_id');
            $this->deviceModelObj->device_manufacture_id = $request->get('device_manufacture_id');
            $this->deviceModelObj->save();

            return $this->sendResponse($this->deviceModelObj->getAllData($request->get('device_manufacture_id')));
        }
        catch(\Exception $e){
            return $this->sendError($e->getMessage(),"Oops.Something was wrong!");
        }
    }

    public function delete(Request $request)
    {
        try {
            $validator = validator($request->all(),[
                'id' => 'required|integer|exists:device_models,id',
                'user_id' => 'required|integer|exists:api_users,id'
            ]);

            if ($validator->fails()) {
                return $this->sendError($validator->errors());
            }

            $this->deviceModelObj = $this->deviceModelObj->getDataById($request->get('id'));

            $this->deviceModelObj->delete();
            return $this->sendResponse([ 'id' => $this->deviceModelObj->id]);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(),"Oops.Something was wrong");
        }
    }
}
