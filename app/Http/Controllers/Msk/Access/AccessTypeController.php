<?php

namespace App\Http\Controllers\Msk\Access;

use App\Http\Controllers\BaseController;
use App\Http\Requests\OneColumnRequest;
use App\Models\Msk\AccessType;
use Illuminate\Http\Request;

class AccessTypeController extends BaseController
{
    private $accessTypeObj;

    public function __construct(AccessType $accessTypeObj)
    {
        $this->accessTypeObj = $accessTypeObj;
    }

    public function index()
    {
        return $this->sendResponse($this->accessTypeObj->getAllData());
    }

    public function save(OneColumnRequest $request)
    {

        // validation process
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        // save process
        if ($request->get('id') == 0) {
            $this->accessTypeObj = new AccessType();
        } // edit process
        else {
            $this->accessTypeObj = $this->accessTypeObj->getDataById($request->get('id'));
        }

        $this->accessTypeObj->name = $request->get('name');
        $this->accessTypeObj->save();

        return $this->sendResponse($this->accessTypeObj->getAllData());
    }

    public function getById(OneColumnRequest $request, $id)
    {
        $validated = $request->validator;

        if ($validated->fails()) {
            return $this->sendError($validated->errors());
        }

        return $this->sendResponse($this->accessTypeObj->getDataById($id));
    }

    public function delete(Request $request)
    {
        $validator = validator($request->all(),[
            'id' => 'required|integer|exists:access_types,id',
            'user_id' => 'required|integer|exists:api_users,id'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $this->accessTypeObj = $this->accessTypeObj->getDataById($request->get('id'));
        try {
            $this->accessTypeObj->delete();
            return $this->sendResponse([ 'id' => $this->accessTypeObj->id]);
        } catch (\Exception $e) {
            return $this->sendError([
                'userMsg' => 'Oops..Something was wrong',
                'devMsg' => $e->getMessage()
            ]);
        }
    }
}
