<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserTokenController extends Controller
{
    public function index(){
        $userTokens = auth()->user()->tokens();
//        dd($userTokens);
        return view('pages.user_tokens');
    }
}
