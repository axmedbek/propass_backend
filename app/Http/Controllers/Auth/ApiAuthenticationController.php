<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Models\ApiUser;
use Illuminate\Http\Request;

class ApiAuthenticationController extends BaseController
{
    private $userApiObj;
    public function __construct(ApiUser $userApiObj)
    {
        $this->userApiObj = $userApiObj;
    }

    public function index(){
        return $this->sendResponse($this->userApiObj->getAllUsers());
    }

    public function login(Request $request){
        $user = $this->userApiObj->findUserByUsername($request->get('username'));
        if ($user){
            if (auth()->attempt($request->only(['username','password']))){
                $tokenObj = $user->createToken('Propass API TOKEN');
                $user['auth_token'] = $tokenObj->accessToken;
                return $this->sendResponse([
                    'status' => true,
                    'user' => $user
                ]);
            }
            else{
                return $this->sendResponse([
                    'status' => false,
                    'message' => 'Password is not correct !'
                ]);
            }
        }
        else{
            return $this->sendResponse([
                'status' => false,
                'message' => 'Username is not correct !'
            ]);
        }
    }

    public function register(Request $request){
        $validator = validator($request->all(),[
            'user_name' => 'required|string',
            'name' => 'required|string',
            'id' => 'required|integer'
        ]);

        if ($validator->fails()){
            return $this->sendResponse($validator->errors());
        }
        else{
            if ($this->userApiObj = $this->userApiObj->findUserByUsername($request->get('user_name'))){
                $this->userApiObj = $this->userApiObj->findUserByUsername($request->get('user_name'));
                $userTokens = $this->userApiObj->tokens;
                foreach ($userTokens as $token){
                    $token->revoke();
                }
                $this->userApiObj->delete();
            }
            $this->userApiObj = new ApiUser();
            $this->userApiObj->name = $request->get('name');
            $this->userApiObj->surname = $request->get('surname');
            $this->userApiObj->username = $request->get('user_name');
            $this->userApiObj->email = $request->get('email');
            $this->userApiObj->userId = $request->get('id');
            $this->userApiObj->save();

            $tokenObj = $this->userApiObj->createToken('Propass API TOKEN');

            $this->userApiObj['auth_token'] = $tokenObj->accessToken;
            return $this->sendResponse([
                'status' => true,
                'user' => $this->userApiObj
            ]);
        }
    }

    public function logout(Request $request){
        return $this->sendResponse([
            'status' => $request->user()->token()->revoke()
        ]);
    }
}
