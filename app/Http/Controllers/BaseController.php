<?php

namespace App\Http\Controllers;

class BaseController extends Controller
{

    public function sendResponse($result, $message = null)
    {
        $response = [
            'status' => true,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }


    public function sendError($devError = null, $userError = null)
    {
        $response = [
            'status' => false,
            'devMessage' => $devError,
            'userMessage' => $userError
        ];

        return response()->json($response);
    }
}
