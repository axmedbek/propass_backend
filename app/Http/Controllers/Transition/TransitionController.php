<?php

namespace App\Http\Controllers\Transition;

use App\Http\Controllers\BaseController;
use App\Models\Transition;
use Illuminate\Http\Request;


class TransitionController extends BaseController
{

    private $transitionObj;
    public function __construct(Transition $transitionObj)
    {
        $this->transitionObj = $transitionObj;
    }

    public function index(){
        return $this->sendResponse($this->transitionObj->getAllTransitions());
    }

    public function addNewTransition(Request $request){
        $validator = validator($request->all(),[
//            'id' => 'nullable|integer|exists:transitions,id',
            'name' => 'required|string',
            'location' => 'required|string',
            'device_type' => 'required|integer|exists:device_types,id',
            'access_type' => 'required|integer|exists:access_types,id',
//            'department_id' => 'required|integer',
            'google_map' => 'required|string'
        ]);

        if ($validator->fails()){
            return $this->sendError($validator->errors());
        }
        else{
            if ($request->get('id') == 0){
                $this->transitionObj = new Transition();
            }
            else{
                $this->transitionObj = $this->transitionObj::find($request->get('id'));
            }

            $this->transitionObj->name = $request->get('name');
            $this->transitionObj->location = $request->get('location');
            $this->transitionObj->device_type_id = $request->get('device_type');
            $this->transitionObj->access_type_id = $request->get('access_type');
            $this->transitionObj->google_map = $request->get('google_map');
            $this->transitionObj->save();

            return $this->sendResponse(['status' => 'ok']);
        }
    }

    public function getTransitionById($id){
        return $this->sendResponse($this->transitionObj->getTransitionById($id));
    }
}
