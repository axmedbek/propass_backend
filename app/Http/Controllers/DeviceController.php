<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Models\DeviceDepartment;
use Illuminate\Http\Request;

class DeviceController extends BaseController
{
    private $deviceObj;
    private $deviceDepartmentObj;

    public function __construct(Device $deviceObj,DeviceDepartment $deviceDepartmentObj)
    {
        $this->deviceObj = $deviceObj;
        $this->deviceDepartmentObj = $deviceDepartmentObj;
    }

    public function index(){
        return $this->sendResponse($this->deviceObj->getAllData());
    }

    public function getDeviceById(Request $request){
        $validator = validator($request->all(),[

        ]);

        if ($validator->fails()){

        }
        else{
            return $this->sendResponse($this->deviceObj->getDataById($request->get('id')));
        }
    }

    public function save(Request $request){
        $validator = validator($request->all(),[
            'device_name' => 'required|string',
            'device_manufacture_name' => 'required|integer|exists:device_manufactures,id',
            'device_model_name' => 'required|integer|exists:device_models,id',
            'device_type_name' => 'required|integer|exists:device_types,id',
            'device_destination_name' => 'required|integer|exists:device_destinations,id',
            'device_ip_address' => 'required|ip',
            'device_google_map' => 'required|string',
            'device_note' => 'nullable|string',
            'departments' => 'required|string'
        ]);

        if ($validator->fails()){
            return $this->sendError($validator->errors());
        }
        else{
//            new device
            if ($request->get('id') == 0){
                $this->deviceObj = new Device();
            }
//            edit device
            else{

            }

            $this->deviceObj->device_name = $request->get('device_name');
            $this->deviceObj->device_manufacture_id = $request->get('device_manufacture_name');
            $this->deviceObj->device_model_id = $request->get('device_model_name');
            $this->deviceObj->device_type_id = $request->get('device_type_name');
            $this->deviceObj->device_destination_id = $request->get('device_destination_name');
            $this->deviceObj->ip_address = $request->get('device_ip_address');
            $this->deviceObj->google_map = $request->get('device_google_map');
            $this->deviceObj->serial_number = $request->get('device_seria_number');
            $this->deviceObj->note = $request->get('device_note');
            $this->deviceObj->save();


            $departments = json_decode($request->get('departments'));
            foreach ($departments as $department){
                $this->deviceDepartmentObj = new DeviceDepartment();
                $this->deviceDepartmentObj->device_id = $this->deviceObj->id;
                $this->deviceDepartmentObj->department_id = $department->value;
                $this->deviceDepartmentObj->structur_name = $department->label;
                $this->deviceDepartmentObj->save();
            }

            return $this->sendResponse(['status' => true]);
        }
    }

    public function getDeviceByDestinationId(Request $request,$id){
        $validator = validator($request->all(),[
//            'id' => 'required|string|exists:device_destinations,id'
        ]);

        if ($validator->fails()){
            return $this->sendError($validator->errors());
        }
        else{
            return $this->sendResponse($this->deviceObj->getDevicesByDestination($id));
        }
    }


    public function getDevicesByDepartmentId(Request $request){
        $validator = validator($request->all(),[
            'reg_id' => 'required|integer|exists:registered_users,id',
            'department_id' => 'required|integer|exists:device_departments,department_id'
        ]);

        if ($validator->fails()){
            return $this->sendError($validator->fails());
        }
        else {
            return $this->sendResponse($this->deviceObj->getDevicesByDepartmentId(
                $request->get('reg_id'),
                $request->get('department_id')));
        }
    }

}
