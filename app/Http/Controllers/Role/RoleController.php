<?php

namespace App\Http\Controllers\Role;

use App\Http\Controllers\BaseController;
use App\Models\Role\Role;
use Illuminate\Http\Request;

class RoleController extends BaseController
{
    private $roleObj;
    public function __construct(Role $roleObj)
    {
        $this->roleObj = $roleObj;
    }

    public function index(){
        return $this->sendResponse($this->roleObj->findAll());
    }

    public function save(Request $request){
        $validator = validator($request->all(),[
            'name' => 'required|string|unique:roles,name,NULL,id,deleted_at,NULL',
            'user_id' => 'required|integer'
        ]);

        if ($validator->fails()){
            return $this->sendResponse([
                'status' => false,
                'errors' => $validator->errors()
            ]);
        }
        else{
            if ($request->get('id') == 0){
                $this->roleObj = new Role();
            }
            else{
                $this->roleObj = $this->roleObj->findById($request->get('id'));
            }

            $this->roleObj->name = $request->get('name');
            $this->roleObj->user_id = $request->get('user_id');
            $this->roleObj->save();

            return $this->sendResponse(['status' => true , 'data' => $this->roleObj->toArray()]);
        }
    }

    public function delete(Request $request){
        $validator = validator($request->all(),[
            'id' => 'required|integer|exists:roles,id',
            'user_id' => 'required|integer'
        ]);

        if ($validator->fails()){
            return $this->sendResponse([
                'status' => false,
                'errors' => $validator->errors()
            ]);
        }
        else{
            $this->roleObj = $this->roleObj->findById($request->get('id'));
            if ($this->roleObj){
                $this->roleObj->deleted_at_user = $request->get('user_id');
                $this->roleObj->save();
                $this->roleObj->delete();
                return $this->sendResponse([
                    'status' => true,
                    'data' => [
                        'id' => $request->get('id')
                    ]
                ]);
            }
            else{
                return $this->sendResponse([
                    'status' => false,
                    'errors' => ['There is no a object']
                ]);
            }
        }
    }
}
