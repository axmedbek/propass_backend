<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('pages.home');
    }

    public function getToken(){
        $user = auth()->user();
        $tokenResult =  $user->createToken('ucuzagetdiapi');
        $token = $tokenResult->token;
        $token->save();
        return response()->json(['access_token' => $tokenResult->accessToken,'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ],200);
    }
}
