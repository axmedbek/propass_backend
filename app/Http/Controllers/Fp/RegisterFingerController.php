<?php

namespace App\Http\Controllers\Fp;

use App\Http\Controllers\BaseController;
use App\Models\Fp\RegisteredFinger;
use Illuminate\Http\Request;

class RegisterFingerController extends BaseController
{

    private $registeredFingerObj;

    public function __construct(RegisteredFinger $registeredFingerObj){
        $this->registeredFingerObj = $registeredFingerObj;
    }

    public function registerFinger(Request $request){
        $valdiator = validator($request->all(),[
            'reg_id' => 'required|integer|exists:registered_users,id',
            'finger_id' => 'required|integer',
            'finger_template' => 'required|string',
            'device_id' => 'required|integer|exists:devices,id'
        ]);

        if ($valdiator->fails()){
            return $this->sendError($valdiator->errors());
        }
        else{
           try{
               $this->registeredFingerObj = new RegisteredFinger();
               $this->registeredFingerObj->reg_id = $request->get('reg_id');
               $this->registeredFingerObj->finger_index = $request->get('finger_id');
               $this->registeredFingerObj->fing_template = $request->get('finger_template');
               $this->registeredFingerObj->device_id = $request->get('device_id');
               $this->registeredFingerObj->is_writed_finger = $request->get('is_writed') ? $request->get('is_writed') : 0;
               $this->registeredFingerObj->save();
               return $this->sendResponse([
                   'status' => 'ok',
                   'finger_id' => $this->registeredFingerObj->id
               ]);
           }catch (\Exception $exception){
               return $this->sendError($exception->getMessage());
           }
        }
    }

    public function getAllFingersLogs(Request $request)
    {
        return $this->sendResponse($this->registeredFingerObj->getAllFingersLogs($request));
    }

    public function getFingerById(Request $request)
    {
        return $this->sendResponse($this->registeredFingerObj->getFingerById($request->get('id')));
    }
    public function getDevicesAndTransitionsWithFingerCode(Request $request){
        return $this->sendResponse($this->registeredFingerObj->getDevicesAndTransitionsWithFingerCode($request->get('finger_template')));
    }
}
