<?php

namespace App\Http\Controllers\Fp;

use App\Http\Controllers\BaseController;
use App\Models\Fp\FpOperations;
use Illuminate\Http\Request;

class FingerOperationController extends BaseController
{
    private $fpOperationObj;

    public function __construct(FpOperations $fpOperationObj)
    {
        $this->fpOperationObj = $fpOperationObj;
    }

    public function getAllOperations(Request $request){
        return $this->sendResponse($this->fpOperationObj->getAllData($request));
    }

    public function addOperation(Request $request){
        $validator = validator($request->all(),[
            'name' => 'required|string',
            'status' => 'required|integer',
            'finger_id' => 'nullable|integer|exists:registered_fingers,id',
            'device_id' => 'required|integer|exists:devices,id',
            'message' => 'nullable|string'
            ]);

        if ($validator->fails()){
            return $this->sendError($validator->errors());
        }
        else {

            $this->fpOperationObj = new FpOperations();
            $this->fpOperationObj->name = $request->get('name');
            $this->fpOperationObj->status = $request->get('status');
            $this->fpOperationObj->message = $request->get('message');
            $this->fpOperationObj->finger_id = $request->get('finger_id');
            $this->fpOperationObj->device_id = $request->get('device_id');
            $this->fpOperationObj->save();

            return $this->sendResponse(['status' => 'ok']);
        }
    }

}
