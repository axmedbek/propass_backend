<?php

namespace App\Http\Controllers\Fp;

use App\Http\Controllers\BaseController;
use App\Models\Fp\FpOperations;
use App\Models\Fp\RegisteredUser;
use Illuminate\Http\Request;

class RegisterUserController extends BaseController
{
    private $registerUserObj;
    private $fpOperationObj;

    public function __construct(RegisteredUser $registeredUserObj,FpOperations $fpOperationObj)
    {
        $this->registerUserObj = $registeredUserObj;
        $this->fpOperationObj = $fpOperationObj;
    }

    public function registerUser(Request $request){
        $validator = validator($request->all(),[
            'uid' => 'required|integer'
        ]);

        if ($validator->fails()){
            return $this->sendError($validator->errors());
        }
        else {
            try{
                if ($this->registerUserObj->findRegIdWithUserId($request->get('uid'))){
                    $this->registerUserObj = RegisteredUser::find($this->registerUserObj->findRegIdWithUserId($request->get('uid')));
                }
                else{
                    $this->registerUserObj = new RegisteredUser();
                    $this->registerUserObj->uid = $request->get('uid');
                    $this->registerUserObj->username = $request->get('username');
                    $this->registerUserObj->save();
                }

                return $this->sendResponse([
                    'status' => 'ok',
                    'reg_id' => $this->registerUserObj->id
                ]);
            }
            catch (\Exception $exception){
                return $this->sendError($exception->getMessage());
            }
        }

    }

    public function getSelectedFingers(Request $request){
        $validator = validator($request->all(),[
            'user_id' => 'required|integer',
            'device_id' => 'required|integer|exists:devices,id'
        ]);

        if ($validator->fails()){
            return $this->sendError($validator->errors());
        }
        else {
            return $this->sendResponse([
                'status' => 'ok',
                'data' => $this->registerUserObj->getSelectedFingers($request->get('user_id'),$request->get('device_id'))
            ]);
        }
    }

    public function getFingerTemplates(Request $request){
        $validator = validator($request->all(),[
            'user_id' => 'required|integer',
            'device_id' => 'required|integer|exists:devices,id',
            'fingerIndex' => 'required|integer',
        ]);

        if ($validator->fails()){
            return $this->sendError($validator->errors());
        }
        else {
            return $this->sendResponse([
                'status' => 'ok',
                'data' => $this->registerUserObj->getFingerTemplates(
                    $request->get('user_id'),
                    $request->get('device_id'),
                    $request->get('fingerIndex'))
            ]);
        }
    }

    public function getRegisteredUserWithFinger(){
        return $this->sendResponse($this->registerUserObj->getRegisteredUsers());
    }

    public function getFingersWithUserId($id){
        return $this->sendResponse($this->registerUserObj->getFingersWithUserId($id));
    }
}
