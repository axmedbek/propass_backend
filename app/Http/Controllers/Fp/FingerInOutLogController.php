<?php

namespace App\Http\Controllers\Fp;

use App\Http\Controllers\BaseController;
use App\Models\Fp\InOutLog;
use Illuminate\Http\Request;

class FingerInOutLogController extends BaseController
{
    private $inOutLogObj;

    public function __construct(InOutLog $inOutLogObj)
    {
        $this->inOutLogObj = $inOutLogObj;
    }

    public function saveInOut(Request $request){
        $validator = validator($request->all(),[
            'reg_id' => 'required|integer|exists:registered_users,id',
            'device_ip' => 'required|ip',
            'destination_id' => 'required|integer|exists:device_destinations,id'
        ]);

        if ($validator->fails()){
            return $this->sendResponse($validator->errors());
        }
        else{
            $this->inOutLogObj = new InOutLog();
            $this->inOutLogObj->reg_id = $request->get('reg_id');
            $this->inOutLogObj->finger_template = $request->get('finger_template');
            $this->inOutLogObj->device_ip = $request->get('device_ip');
            $this->inOutLogObj->card_number = $request->get('card_number');
            $this->inOutLogObj->destination_id = $request->get('destination_id');
            $this->inOutLogObj->save();

            return $this->sendResponse(['status' => true]);
        }
    }
}
