<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class DeviceModelRequest extends FormRequest
{

    public $validator = null;

    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'name' => 'required|string|max:255|min:2',
            'device_sdk_type_id' => 'required|integer|exists:device_sdk_types,id',
            'device_finger_code_id' => 'required|integer|exists:device_finger_codes,id',
            'device_manufacture_id' => 'required|integer|exists:device_manufactures,id',
            'user_id' => 'required|integer|exists:api_users,id'
        ];
    }
}
