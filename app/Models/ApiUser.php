<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

/**
 * @property string password
 */
class ApiUser extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','username','password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function findUserByUsername($username){
        return $this::where('username',$username)->first();
    }

    public function getAllUsers(){
        return $this::get(['id','name','surname','email','created_at','username']);
    }

    public function checkApiUser($uid){
        return $this::where('userId',$uid)->first();
    }
}
