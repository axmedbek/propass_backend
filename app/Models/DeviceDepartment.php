<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceDepartment extends Model
{
    public $timestamps = false;
}
