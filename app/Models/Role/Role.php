<?php

namespace App\Models\Role;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    public function findById($id){
        return $this::find($id);
    }

    public function findAll(){
        return $this::get(['id','name']);
    }
}
