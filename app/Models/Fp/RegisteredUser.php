<?php

namespace App\Models\Fp;

use Illuminate\Database\Eloquent\Model;

class RegisteredUser extends Model
{
    public function findRegIdWithUserId($uid){
        $regObj = $this::where('uid',$uid)->first();
        return $regObj ? $regObj->id : null;
    }

    public function getSelectedFingers($user_id,$device_id){
        $data = $this::join('registered_fingers','registered_fingers.reg_id','registered_users.id')
            ->join('fp_operations','registered_fingers.id','fp_operations.finger_id')
            ->where('registered_users.uid',$user_id)->where('fp_operations.device_id',$device_id)
            ->groupBy('registered_fingers.finger_index')->get(['registered_fingers.finger_index']);
        return $data;
    }

    public function getFingerTemplates($user_id,$device_id,$fingerIndex){
        $data = $this::join('registered_fingers','registered_fingers.reg_id','registered_users.id')
            ->join('fp_operations','registered_fingers.id','fp_operations.finger_id')
            ->where('registered_users.uid',$user_id)
            ->where('registered_fingers.finger_index',$fingerIndex)
            ->where('fp_operations.device_id',$device_id)
            ->get();
        return $data;
    }

    public function getRegisteredUsers(){
        return $this::join('registered_fingers','registered_fingers.reg_id','registered_users.id')
            ->groupBy('registered_users.id','registered_users.username','registered_users.position')
            ->get(['registered_users.id','registered_users.username','registered_users.position']);
    }

    public function getFingersWithUserId($id){
        return $this::join('registered_fingers','registered_fingers.reg_id','registered_users.id')
            ->where('registered_users.id',$id)
            ->where('is_writed_finger',0)
            ->get();
    }
}
