<?php

namespace App\Models\Fp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RegisteredFinger extends Model
{

    public function getAllFingersLogs($request){
        $data  = $this::leftJoin('registered_users','registered_users.id','registered_fingers.reg_id')
            ->join('devices','devices.id','registered_fingers.device_id')
            ->join('device_models','device_models.id','devices.device_model_id')
            ->join('device_sdk_types','device_sdk_types.id','device_models.device_sdk_type_id');

//        if ($request->get('operation')){
//            $data = $data->where('device_finger_operations.slug',$request->get('operation'));
//        };

        return $data->orderByDesc('registered_fingers.created_at')
            ->select([
                'registered_fingers.*',
                'device_sdk_types.name as sdk_type',
                'registered_users.username as username'
            ])->paginate(10);
    }

    public function getFingerById($id){
        $data  = $this::leftJoin('registered_users','registered_users.id','registered_fingers.reg_id')
            ->join('devices','devices.id','registered_fingers.device_id')
            ->join('device_models','device_models.id','devices.device_model_id')
            ->join('device_sdk_types','device_sdk_types.id','device_models.device_sdk_type_id')
            ->where('registered_fingers.id',$id)
            ->select([
                'registered_fingers.*',
                'device_sdk_types.name as sdk_type',
                'registered_users.username as username'
            ])->first();

        return $data;
    }

    public function getDevicesAndTransitionsWithFingerCode($id){
        $data = $this::join('devices','registered_fingers.device_id','devices.id')
            ->leftJoin('transition_devices','transition_devices.device_id','devices.id')
            ->leftJoin('transitions','transition_devices.transition_id','transitions.id')
            ->where('registered_fingers.fing_template','like',$id)
            ->get(['registered_fingers.id','devices.device_name as device_name','transitions.name as transition_name']);

        $transitions = $this::join('devices','registered_fingers.device_id','devices.id')
            ->leftJoin('transition_devices','transition_devices.device_id','devices.id')
            ->leftJoin('transitions','transition_devices.transition_id','transitions.id')
            ->where('registered_fingers.fing_template','like',$id)->whereNotNull('transitions.id')->count();

        return [
            'data' => $data,
            'transitionCount' => $transitions
        ];
    }

}
