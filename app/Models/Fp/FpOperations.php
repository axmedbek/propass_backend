<?php

namespace App\Models\Fp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class FpOperations extends Model
{
    public function getDataById($id)
    {
        return $this::find($id);
    }

    public function getAllData($request){
        $data  = $this::join('devices','devices.id','fp_operations.device_id')
                        ->leftJoin('device_finger_operations','device_finger_operations.slug','fp_operations.name')
                        ->whereIn('fp_operations.status',$request->get('status') ? [$request->get('status')] : [1,2]);
        if ($request->get('operation')){
            $data = $data->where('device_finger_operations.slug',$request->get('operation'));
        };
        if ($request->get('device')){
            $data = $data
                ->where(
                    'devices.id',
                    $request->get('device')
                );
        };
        return $data->orderByDesc('created_at')
            ->select(['fp_operations.*','devices.ip_address','device_finger_operations.name as operation_name'])->paginate(10);
    }

}
