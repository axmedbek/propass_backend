<?php

namespace App\Models\Msk;

use Illuminate\Database\Eloquent\Model;

class DeviceType extends Model
{
    public function getAllDeviceTypes(){
        return $this::all();
    }

    public function getDeviceTypeById($id){
        return $this::find($id);
    }
}
