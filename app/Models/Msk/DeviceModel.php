<?php

namespace App\Models\Msk;

use Illuminate\Database\Eloquent\Model;

class DeviceModel extends Model
{


    public function getDataById($id)
    {
        return $this::find($id);
    }

    public function getAllData($id){

        $returnedData = [];

        $coreData =  $this::with('get_device_sdk')
            ->with('get_device_finger_code')->where('device_manufacture_id',$id) ->get();

        foreach ($coreData as $datum){
            $returnedData[] = [
                'id' => $datum['id'],
                'name' => $datum['name'],
                'sdk_types' => [
                    'value' => $datum['get_device_sdk']['id'],
                    'label' => $datum['get_device_sdk']['name'],
                ],
                'finger_codes_types' => [
                    'value' => $datum['get_device_finger_code']['id'],
                    'label' => $datum['get_device_finger_code']['name'],
                ]
            ] ;
        }

        return $returnedData;
    }

    public function get_device_sdk(){
        return $this->belongsTo(DeviceSdkType::class,'device_sdk_type_id');
    }

    public function get_device_finger_code(){
        return $this->belongsTo(DeviceFingerCode::class,'device_finger_code_id');
    }

    public function get_device_manufacture(){
        return $this->belongsTo(DeviceManufacture::class,'device_manufacture_id');
    }
}
