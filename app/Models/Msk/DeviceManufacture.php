<?php

namespace App\Models\Msk;

use Illuminate\Database\Eloquent\Model;

class DeviceManufacture extends Model
{
    public function getAllManufactures(){
        return $this::all();
    }

    public function getManufactureById($id){
        return $this::find($id);
    }
}
