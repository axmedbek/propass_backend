<?php

namespace App\Models\Msk;

use Illuminate\Database\Eloquent\Model;

class DeviceFingerOperation extends Model
{
    public function getAllFingerOperation(){
        return $this::all();
    }

    public function getFingerOperationById($id){
        return $this::find($id);
    }
}
