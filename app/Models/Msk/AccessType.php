<?php

namespace App\Models\Msk;

use Illuminate\Database\Eloquent\Model;

class AccessType extends Model
{
    public function getAllData(){
        return $this::all();
    }

    public function getDataById($id){
        return $this::find($id);
    }
}
