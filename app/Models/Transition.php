<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transition extends Model
{
    public function getAllTransitions()
    {
        return $this::join('access_types', 'access_types.id', 'transitions.access_type_id')
            ->get(['transitions.id as id', 'transitions.name as name', 'access_types.name as access_type']);
    }

    public function getTransitionById($id){
        return $this::join('access_types', 'access_types.id', 'transitions.access_type_id')
            ->join('device_types', 'device_types.id', 'transitions.device_type_id')
            ->where('transitions.id',$id)->first([
                'transitions.*',
                'access_types.name as access_type_name',
                'device_types.name as device_type_name',
            ]);
    }
}
