<?php

namespace App\Models;

use App\Models\Fp\RegisteredFinger;
use App\Models\Msk\DeviceType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
    use SoftDeletes;

    public function getAllData()
    {
        $devices = $this::join('device_types', 'device_types.id', 'devices.device_type_id')
            ->get([
                'devices.id as id',
                'devices.device_name as device_name',
                'device_types.name as device_type_name',
                'devices.ip_address as ip_address']);
        return $devices;
    }

    public function getDataById($id)
    {
        $device = $this::join('device_manufactures', 'device_manufactures.id', 'devices.device_manufacture_id')
            ->join('device_models', 'device_models.id', 'devices.device_model_id')
            ->join('device_finger_codes', 'device_finger_codes.id', 'device_models.device_finger_code_id')
            ->join('device_sdk_types', 'device_sdk_types.id', 'device_models.device_sdk_type_id')
            ->join('device_destinations', 'device_destinations.id', 'devices.device_destination_id')
            ->join('device_types', 'device_types.id', 'devices.device_type_id')
            ->where('devices.id', $id)
            ->first(['devices.*', 'device_manufactures.name as manufacture',
                'device_models.name as model', 'device_finger_codes.name as finger', 'device_sdk_types.name as sdk_type',
                'device_destinations.name as destination', 'device_types.name as type']);
        return $device;
    }

    public function get_device_type()
    {
        return $this->belongsTo(DeviceType::class, 'device_type_id');
    }

    public function getIpWithId($id)
    {
        return $this::find($id) ? $this::find($id)->ip : 0;
    }

    public function getDevicesByDestination($destinationId){
        return $this::join('device_destinations', 'device_destinations.id', 'devices.device_destination_id')
            ->where('device_destinations.id',$destinationId)
            ->get(['devices.id as id','devices.device_name as name','device_destinations.name as destination_name']);
    }

    public function getDevicesByDepartmentId($reg_id,$department_id){
        $ip_addresses = $this::join('device_departments','device_departments.device_id','devices.id')
            ->leftJoin('registered_fingers','registered_fingers.device_id','devices.id')
            ->where('device_departments.department_id',$department_id)
            ->whereNull('registered_fingers.reg_id')
            ->groupBy('devices.ip_address')
            ->get(['devices.ip_address']);

        $finger_templates = RegisteredFinger::where('reg_id',$reg_id)->where('is_writed_finger',0)->get(['fing_template','finger_index']);

        $total_data = [
            'reg_id' => $reg_id,
            'templates' => $finger_templates,
            'ip_addresses' => $ip_addresses
        ];
        return $total_data;
    }
}
